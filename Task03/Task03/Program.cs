﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
    class Program
    {
        static void Main(string[] args)
        {
            calculate();
        }

        static void calculate(int a = 5)
        {
            Console.WriteLine($"The total of {a} + {a} = {a + a}");
            Console.WriteLine($"The total of {a} - {a} = {a - a}");
            Console.WriteLine($"The total of {a} / {a} = {a / a}");
            Console.WriteLine($"The total of {a} * {a} = {a * a}");
        }
    }
}
